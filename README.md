# What's this?

A repository holding issues for each commit that went into Qt dev branches that had a Pick-to: 5.15 or 5.12 and has not landed as a cherry-pick in kde/5.15 branches.

And the script used to create such issues automatically.

# Should we backport all those issues?

Not necessarily, but it's good to have an inventory.
